function initGame()
{
	trace('simulador', 'Creant instància del joc...', 'gold');
	$.post('views/ajax_actions/init_game.php', {}, function(data){
		if(data == 'INIT OK')
		{
			trace('kPAX', 'Joc iniciat correctament.', 'lightgreen');
		}
		else
		{
			trace('kPAX', "El joc no s\'ha pogut iniciar. Probablement la última vegada no va ser tancat correctament. ", 'salmon');
		}
		checkLeagues();
	});
}

function endGame()
{
	trace('simulador', 'Tancant joc...', 'gold');
	$.post('views/ajax_actions/end_game.php', {}, function(data){
		if(data == 'INIT OK')
		{
			trace('kPAX', 'Joc tancat correctament.', 'lightgreen');
			window.location="?act=";
		}
		else
		{
			trace('kPAX', "El joc no s\'ha pogut tancar.", 'salmon');
		}
	});
}

function listAchievements()
{
	trace('simulador', 'Llistant assoliments...', 'gold');
	$.post('views/ajax_actions/list_achievements.php', {}, function(data){
		trace('kPAX', 'Llista d\'assoliments:<br/>' + data, 'lightgreen');
	});
}

function unlockAchievement()
{
	var achId = document.unlockach.achid.value;
	var level = document.unlockach.level.value;

	trace('simulador', 'Desbloquejant assoliment ' + achId + '...', 'gold');
	$.post('views/ajax_actions/unlock_achievement.php', 
		{
			achId: achId,
			level: level
		}, function(data){
		var parts = data.split('#SEP#');
		if(parts[0] == 'OK')
		{
			trace('kPAX', 'Assoliment aconseguit.', 'lightgreen');
			showAlert('Assoliment Desbloquejat', parts[2], 'http://localhost/elgg-1.8.14/mod/kpax_achievements/icondirect.php?achid=' + parts[1] + '&size=small');
		}
		else
		{
			trace('kPAX', "Error al desbloquejar: " + parts[0], 'salmon');
		}
	});
}

function checkLeagues()
{
	trace('simulador', 'Buscant competicions disponibles per al joc seleccionat...', 'gold');
	$.post('views/ajax_actions/check_leagues.php', {}, function(data){
		trace('kPAX', 'Llistat de competicions disponibles completat.', 'lightgreen');
		if(data != '')
		{
			var leagues = data.split('#LSEP#');

			for(var i in leagues)
			{
				var ldata = leagues[i].split("#SEP#");
				showAlert('Competició Disponible', ldata[1], 'http://localhost/elgg-1.8.14/mod/kpax_leagues/icondirect.php?idLeague=' + ldata[0] + '&size=small');
			}
		}
	});
}

function listLeagues()
{
	trace('simulador', 'Llistant competicions...', 'gold');
	$.post('views/ajax_actions/list_leagues.php', {}, function(data){
		trace('kPAX', 'Llista de competicions:<br/>' + data, 'lightgreen');
	});
}

function submitLeagueScore()
{
	var idLeague = document.submitleague.idLeague.value;
	var score = document.submitleague.score.value;

	trace('simulador', 'Enviant puntuació a la competició ' + idLeague + '...', 'gold');
	$.post('views/ajax_actions/submit_league_score.php', 
		{
			idLeague: idLeague,
			score: score
		}, function(data){
		if(data == 'OK')
		{
			trace('kPAX', 'Puntuació enviada correctament.', 'lightgreen');
		}
		else
		{
			trace('kPAX', "Error al guardar la puntuació: " + data, 'salmon');
		}
	});
}