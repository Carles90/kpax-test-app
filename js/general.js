alertId = 0;

function showAlert(title, text, icon)
{
	$.post('views/window_alert.php', {title: title, text: text, icon: icon}, function(data){
		var tempId = alertId;
		alertId++;

		var iDiv = document.createElement('div');
		iDiv.id = 'window_alert_' + tempId;
		iDiv.innerHTML = data;

		var dDiv = document.getElementById('window_alerts');
		dDiv.appendChild(iDiv);

		$("#window_alert_" + tempId).hide();
		$("#window_alert_" + tempId).fadeIn('slow');

		setTimeout(function(){
			$("#window_alert_" + tempId).fadeOut('slow');
		}, 10000);
	});
}

function trace(username, text, color)
{
	var console = document.getElementById('game_console');
	console.innerHTML = console.innerHTML + '<font color="white">' + username + '@kpax &Gt; </font><font color="' + color + '">' + text + "</font><br/>";
	console.scrollTop += 1000000;
}