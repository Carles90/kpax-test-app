<?php
include('oauth.php');
include('kpax_oauth.php');
session_start();

if(isset($_SESSION["testappSession"]))
{
	$objKpax = new kpaxSrv($_SESSION['testappUsername']);
	if(isset($_SESSION['testappGame']))
	{
		$curGame = $objKpax->getGame($_SESSION['testappGame'], $_SESSION['testappSession']);
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
	<head>
		<title>kPAX :: Simulació</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="style/style.css" />

		<script src="js/jquery.js"></script>
		<script src="js/general.js"></script>
		<script src="js/ajax.js"></script>
	</head>
	<body>
		<div class="header">
			<h1>kPAX Simulació</h1>
			<h2>Joc de proves mestre</h2>
			<h3>Sessió iniciada com: <b><?php echo(isset($_SESSION['testappUsername']) ? $_SESSION['testappUsername'] : 'Cap') ?></b></h3>
			<h3>Identificador de sessió: <b><?php echo(isset($_SESSION['testappSession']) ? $_SESSION['testappSession'] : 'Cap') ?></b></h3>
			<h3>Operant amb el joc: <b><?php echo(isset($curGame) ? $curGame->name : 'Cap') ?></b></h3>
			<h3>Secret del joc: <b><?php echo(isset($curGame) ? $curGame->secretGame : 'Cap') ?></b></h3>
			<h4><a href="?act=">Inici</a> :: <a href="?act=logout">Tancar Sessió</a></h4>
		</div>
		<div class="content">
			<div id="window_alerts"></div>
		