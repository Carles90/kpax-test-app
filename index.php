<?php
include('global.php');

$act = '';

if(isset($_GET['act']))
{
	$act = $_GET['act'];
}

if(isset($_SESSION['testappSession']))
{
	switch($act)
	{
		case "test":
			include('views/test.php');
		break;
		case "gameslist":
			include('views/gameslist.php');
		break;
		case "gameselect":
			include('views/gameselect.php');
		break;
		case "startgame":
			include('views/startgame.php');
		break;
		case "unlock_achievement":
			include('views/unlock_achievement.php');
		break;
		case "submit_league_score":
			include('views/submit_league_score.php');
		break;
		case "logout":
			include('views/logout.php');
		break;
		default:
			include('views/menu.php');
		break;
	}
}
else
{
	if($act == "auth")
	{
		include('views/auth.php');
	}
	else
	{
		include('views/login.php');
	}
}

include('footer.php');
?>