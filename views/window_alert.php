<?php
$title = $_POST['title'];
$text = $_POST['text'];
$icon = $_POST['icon'];
?>

<div class="window_alert">
	<h1><?php echo($title); ?></h1>
	<div class="window_alert_image" style="background-image: url('<?php echo($icon); ?>');"></div>
	<p><?php echo($text); ?></p>
</div>