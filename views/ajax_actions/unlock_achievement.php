<?php
include('ajax_global.php');

$id = (int)$_POST['achId'];
$level = (int)$_POST['level'];

$response = $objKpax->unlockAchievement($_SESSION['testappSession'], $curGame->secretGame, $id, $level);

echo($response.'#SEP#');

if($response == 'OK')
{
	$achInfo = $objKpax->getAchievement($id);

	$name = $achInfo->name;

	if($achInfo->maxLevel != 0)
	{
		$name .= ' ('.number_format($level, 0, ',', '.').'/'.number_format($achInfo->maxLevel, 0, ',', '.').')';
	}

	echo($id.'#SEP#'.$name);
}
?>