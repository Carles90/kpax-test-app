<?php
include('ajax_global.php');

$leagueList = $objKpax->getLeagues("running");
$userLeagues = $objKpax->getUserLeagues($_SESSION['testappSession']);

$infoStr = '';

foreach($userLeagues as $ul)
{
	foreach($leagueList as $l)
	{
		if($l->idLeague == $ul->idLeague)
		{
			$response = '';

			if($l->scoreType == 'tree')
			{
				$response = $objKpax->checkLeagueSubmitScore($_SESSION['testappSession'], $curGame->secretGame, $l->idLeague, $l->distribution, 'treecheck');
			}
			else
			{
				$response = $objKpax->checkLeagueSubmitScore($_SESSION['testappSession'], $curGame->secretGame, $l->idLeague, $l->distribution, 'check');
			}

			if($response == 'OK')
			{
				if($infoStr != '')
				{
					$infoStr .= '#LSEP#';
				}

				$infoStr .= $l->idLeague.'#SEP#'.$l->title;
			}
		}
	}
}

echo($infoStr);
?>