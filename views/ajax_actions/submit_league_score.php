<?php
include('ajax_global.php');

$idLeague = (int)$_POST['idLeague'];
$score = (int)$_POST['score'];

$objLeague = $objKpax->getLeague($_SESSION['testappSession'], $idLeague);

$resp = "NOT_IMPLEMENTED";

if($objLeague->distribution == "single")
{
	if($objLeague->scoreType == "scoretable")
	{
		$resp = $objKpax->submitLeagueSingleScoretable($_SESSION['testappSession'], $curGame->secretGame, $idLeague, $score);
	}
	elseif($objLeague->scoreType == "knockout")
	{
		$resp = $objKpax->submitLeagueSingleKnockout($_SESSION['testappSession'], $curGame->secretGame, $idLeague, $score);
	}
	elseif($objLeague->scoreType == "tree")
	{
		$resp = $objKpax->submitLeagueSingleTree($_SESSION['testappSession'], $curGame->secretGame, $idLeague, $score);
	}
}
else
{
	if($objLeague->scoreType == "scoretable")
	{
		$resp = $objKpax->submitLeagueTeamScoretable($_SESSION['testappSession'], $curGame->secretGame, $idLeague, $score);
	}
	elseif($objLeague->scoreType == "knockout")
	{
		$resp = $objKpax->submitLeagueTeamKnockout($_SESSION['testappSession'], $curGame->secretGame, $idLeague, $score);
	}
	elseif($objLeague->scoreType == "tree")
	{
		$resp = $objKpax->submitLeagueTeamTree($_SESSION['testappSession'], $curGame->secretGame, $idLeague, $score);
	}
}

echo($resp);
?>