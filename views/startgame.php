<?php
if(isset($_SESSION['testappGame']))
{
	?>
	<h1>Accions disponibles</h1>

	<table cellspacing="5" class="game_actions">
		<tr>
			<td>
				<h2>Funcionalitat</h2>
				<ul>
					<li><a href="javascript:void(0)" onclick="endGame()">Tancar joc</a></li>
				</ul>
			</td>
			<td>
				<h2>Assoliments</h2>
				<ul>
					<li><a href="javascript:void(0)" onclick="listAchievements()">Llistar</a></li>
				</ul>
				<h3>Desbloquejar assoliment</h3>
				<form name="unlockach" method="post" action="javascript:void(0)">
					<table class="game_actions_form">
						<tr>
							<td>ID: </td>
							<td><input type="text" name="achid" /></td>
						</tr>
						<tr>
							<td>Nivell: </td>
							<td><input type="text" name="level" value="0" /></td>
						</tr>
					</table>
					<input type="button" value="Desbloquejar" onclick="unlockAchievement()" />
				</form>
			</td>
			<td>
				<h2>Competicions</h2>
				<ul>
					<li><a href="javascript:void(0)" onclick="listLeagues()">Llistar totes</a></li>
				</ul>
				<h3>Enviar puntuació</h3>
				<form name="submitleague" method="post" action="javascript:void(0)">
					<table class="game_actions_form">
						<tr>
							<td>ID Competició: </td>
							<td><input type="text" name="idLeague" /></td>
						</tr>
						<tr>
							<td>Puntuació: </td>
							<td><input type="text" name="score" value="0" /></td>
						</tr>
					</table>
					<input type="button" value="Enviar" onclick="submitLeagueScore()" />
				</form>
			</td>
		</tr>
	</table>
	<?php
}

?>

<h1>Consola</h1>
<div id="game_console">
	
</div>

<?php
if(!isset($_SESSION['testappGame']))
{
	?>
	<script>
	trace("simulador", 'No s\'ha seleccionat cap joc amb el que actuar.', 'salmon');
	</script>
	<?php
}
else
{
	?>
	<script>
	initGame();
	</script>
	<?php
}
?>